//
//  CustomCells.swift
//  Trinity2
//
//  Created by Desai, Rikin on 6/7/15.
//  Copyright (c) 2015 Desai, Rikin. All rights reserved.
//

import Foundation

//Customs Cells for Displaying in a TableView

class RatingCell: UITableViewCell{
    @IBOutlet weak var venueTitle: UILabel!
    @IBOutlet weak var venueAddress: UILabel!
    @IBOutlet weak var venueRating: UILabel!
    
}
class NonRatingCell: UITableViewCell{
    @IBOutlet weak var venueTitle: UILabel!
    @IBOutlet weak var venueAddress: UILabel!
}
