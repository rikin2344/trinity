//
//  DataManager.swift
//  Trinity2
//
//  Created by Desai, Rikin on 6/7/15.
//  Copyright (c) 2015 Desai, Rikin. All rights reserved.
//

import Foundation

/**
*  Class deals with downloading data through four square API
*/
class DataManager: NSObject{

    static var sharedManager = DataManager()
    private var timeLineData: [ [String: String] ]?
    weak var signalManager = SignalManager.sharedInstance
    var processComplete = true

    override init(){
        super.init()
        self.signalManager?.emitLocationSignal.subscribeNext({ (coordinates) -> Void in
            let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
            dispatch_async(dispatch_get_global_queue(priority, 0)) {
                if self.processComplete{
                    self.getPlacesDataWithCoordinate(coordinates as! [String: String])
                }
            }
        })
    }
    
    //MARK: PUBLIC FUNCTIONS
    
    func getTimelineData()->[ [String: String] ]?{
        return timeLineData
    }

    //MARK: PRIVATE FUNCTIONS --------
    
    //MARK: Data Builder
    
    private func getPlacesDataWithCoordinate(coordinates: [String: String]){
        println("Data Downloading....")
        self.timeLineData = []
        self.processComplete = false
        var URL = buildURlForVenueDetailsWithCoordinates(coordinates)
        let resultDataDictionary = NSData(contentsOfURL: URL!)
        
        var error: NSError?
        let dictionary: [NSObject: AnyObject] = NSJSONSerialization.JSONObjectWithData(resultDataDictionary!, options: NSJSONReadingOptions.MutableContainers, error: &error) as! [NSObject: AnyObject]
        self.signalManager!.statusText.sendNext("Downloading Venue Rating...")
        if (error != nil) {
            println(error)
            return
        }
        else {
            self.parseVenueDataFromDictionary(result: dictionary)
        }
    }
    
    private func getVenueRatingData(){
        var tempTimeLineData = self.timeLineData!
        for (index,venue) in enumerate(tempTimeLineData){
            let venueID = venue[kID]! as String
            var URL = buildURLForVenueRatingWithVenueID(venueID)
            let resultDataDictionary = NSData(contentsOfURL: URL!)
            
            var error: NSError?
            let dictionary: [NSObject: AnyObject] = NSJSONSerialization.JSONObjectWithData(resultDataDictionary!, options: NSJSONReadingOptions.MutableContainers, error: &error) as! [NSObject: AnyObject]
            
            if (error != nil) {
                println(error)
                return
            }
            else {
                var rating = self.parseDataForVenueRating(dictionary)
                (self.timeLineData![index]).updateValue(rating, forKey: kRating)
            }
        }
        
        self.signalManager?.updateTableView.sendNext(timeLineData)
    }
    
    //MARK: JSON Parser
    private func parseDataForVenueRating(result: [NSObject: AnyObject])->String{
        return result.getRatingFromData() ?? kDefaultRatingText
    }
    
    private func parseVenueDataFromDictionary(#result: [NSObject: AnyObject]){
        self.timeLineData = [ [String: String] ]()
        for (index, venue) in enumerate(result.getVenueArray()){
            let venue = venue as! [NSObject: AnyObject]
            var tempDict = [String: String]()
            tempDict[kName] = venue.getName()!
            tempDict[kFormattedAddress] = venue.getFormattedAddress()!
            tempDict[kID] = venue.getVenueID()!
            self.timeLineData?.append(tempDict)
        }
        
        self.getVenueRatingData()
        
    }
    
    //MARK: URL Builder
    private func buildURlForVenueDetailsWithCoordinates(coordinates: [String: String])->NSURL?{
        var baseURL = "https://api.foursquare.com/v2/venues/search?client_id="
        if let path = NSBundle.mainBundle().pathForResource("Info", ofType: "plist"),
            myDict = NSDictionary(contentsOfFile: path) {
                var clientID = myDict.objectForKey("ClientID") as! String
                var secretClient = myDict.objectForKey("ClientSecretKey") as! String
                var URL = "\(baseURL)\(clientID)&client_secret=\(secretClient)&v=20130815&ll=\(coordinates[kLatitude]!),\(coordinates[kLongitude]!)"
                return NSURL(string:URL.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
        }
        return nil
    }
    
    private func buildURLForVenueRatingWithVenueID(id: String)->NSURL?{
        var baseURL = "https://api.foursquare.com/v2/venues/"
        if let path = NSBundle.mainBundle().pathForResource("Info", ofType: "plist"),
            myDict = NSDictionary(contentsOfFile: path) {
                var clientID = myDict.objectForKey("ClientID") as! String
                var secretClient = myDict.objectForKey("ClientSecretKey") as! String
                var URL = "\(baseURL)\(id)?client_id=\(clientID)&client_secret=\(secretClient)&v=20130815&ll)"
                return NSURL(string:URL.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
        }
        return nil
        
    }

    //MARK: Reactive Cocoa - Signals
    private func sendSignalToNotifyCompletion(){
    }
}