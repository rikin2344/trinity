//
//  localizedString.swift
//  Trinity2
//
//  Created by Desai, Rikin on 6/7/15.
//  Copyright (c) 2015 Desai, Rikin. All rights reserved.
//

import Foundation


let kLatitude = "latitude"
let kLongitude = "longitude"

let kName = "name"
let kFormattedAddress = "formattedAddress"
let kRating = "rating"
let kID = "id"

let kDefaultRatingText = "No Rating Available"

let kRatingCell = "ratingCell"
let kNonRatingCell = "nonRatingCell"