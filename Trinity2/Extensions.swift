//
//  Extensions.swift
//  Trinity2
//
//  Created by Desai, Rikin on 6/7/15.
//  Copyright (c) 2015 Desai, Rikin. All rights reserved.
//

import Foundation


extension Dictionary{
    func getVenueArray()->[AnyObject]{
        var result = self.values
        return ((result.array[1] as! [NSObject: AnyObject])["venues"] as! [AnyObject])
    }

    func getName()->String?{
        for (key, value) in self{
            if (key as! String) == "name"{
                return value as? String
            }
        }
        return nil
    }
    
    func getFormattedAddress()->String?{
        for (key, value) in self{
            if (key as! String) == "location"{
                let value = value as! [NSObject: AnyObject]
                return toString(value["formattedAddress"]! as! [AnyObject])
            }
        }
        return nil
    }
    
    func getVenueID()->String?{
        for (key, value) in self{
            if (key as! String) == "id"{
                return value as? String
            }
        }
        return nil
    }
    
    func getRatingFromData() -> String?{
        var result = self.values
        if let rating = (  (((result.array[1] as! [NSObject: AnyObject])["venue"] as! [NSObject: AnyObject])["rating"] as? Float  )) {
            return toString(rating)
        }
        return nil
    }
}
