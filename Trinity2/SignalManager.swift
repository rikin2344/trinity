//
//  SignalManager.swift
//  Trinity2
//
//  Created by Desai, Rikin on 6/7/15.
//  Copyright (c) 2015 Desai, Rikin. All rights reserved.
//

import Foundation

//Creates and manages Signals Throughout the application
class SignalManager: NSObject{
    
    var emitLocationSignal: RACSignal!
    private var newLocation = RACSubject()
    
    var updateTableSignal: RACSignal!
    var updateTableView = RACSubject()
    
    var statusSignal: RACSignal!
    var statusText = RACSubject()
    
    private var coordinates = [[String: String]]()
    static var sharedInstance = SignalManager()
    
    override init(){
        super.init()
        self.emitLocationSignal = newLocation
        self.updateTableSignal = updateTableView
        self.statusSignal = statusText
        
        //hardcoded coordinates (around paris)
        self.coordinates.append([kLatitude:"48.8582", kLongitude: "2.2945"])
        self.coordinates.append([kLatitude:"48.8634916", kLongitude: "2.327494300000012"])
        self.coordinates.append([kLatitude:"48.836783", kLongitude: "2.3517799999999625"])
        self.coordinates.append([kLatitude:"48.846306", kLongitude: "2.278579000000036"])
        self.coordinates.append([kLatitude:"48.845143", kLongitude: "2.2662179999999807"])
        self.coordinates.append([kLatitude:"48.833986", kLongitude: "2.244819000000007"])

        self.updateLocation()
        var updateLocationTimer = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: "updateLocationWithDelay:", userInfo: nil, repeats: true)
    }
    
    //Fire a single signal to update the location at app start
    private func updateLocation(){
        let randomNumber = Int(arc4random_uniform(UInt32(coordinates.count)))
        self.newLocation.sendNext(self.coordinates[randomNumber])
    }
    
    //Continuously fires signal at some time interval
    //Signals are processed if there is no prior signals processing
    func updateLocationWithDelay(timer: NSTimer){
        let randomNumber = Int(arc4random_uniform(UInt32(coordinates.count)))
        self.newLocation.sendNext(self.coordinates[randomNumber])
    }
}

