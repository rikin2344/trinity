//
//  ViewController.swift
//  Trinity2
//
//  Created by Desai, Rikin on 6/7/15.
//  Copyright (c) 2015 Desai, Rikin. All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var statusLabel: UILabel!
    
    private var animationDone = false
    private var timeLineData: [[String: String]]?
    weak var signalManager = SignalManager.sharedInstance
    weak var dataManager = DataManager.sharedManager
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.alpha = 0
        self.tableView.hidden = true
        self.backgroundView.hidden = false
        
        self.signalManager!.updateTableView.subscribeNext({ (object) -> Void in
            self.showData(object)
        })
        
        self.signalManager!.statusSignal.subscribeNext { (status) -> Void in
            dispatch_async(dispatch_get_main_queue()) {
                if let status = status as? String{
                    self.statusLabel.text = status
                }
            }
        }
        
        var timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "startLoadingIndicator:", userInfo: nil, repeats: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UI Updates
    
    func startLoadingIndicator(timer: NSTimer){
        self.loadingIndicator.startAnimating()
        timer.invalidate()
    }
    
    
    func showData(data: AnyObject!){
        if self.animationDone == false{
            self.animationDone = true
            self.signalManager!.statusText.sendNext("Displaying Data...")

            var dispatchTime: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(0.7 * Double(NSEC_PER_SEC)))
            dispatch_after(dispatchTime, dispatch_get_main_queue(), {
                UIView.animateWithDuration(1.5, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                    self.loadingIndicator.stopAnimating()
                    self.loadingIndicator.hidden = true
                    self.backgroundView.alpha = 0
                    self.tableView.alpha = 1
                    self.backgroundView.hidden = true
                    self.tableView.hidden = false
                }, completion: nil)
            })
        }
        
        let data = data as! [[String: String]]
        self.timeLineData = data
        dispatch_async(dispatch_get_main_queue()) {
            self.tableView.reloadData()
            self.dataManager?.processComplete = true
        }

    }

    //MARK: TableView Delegate/Dataosource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.timeLineData != nil && self.timeLineData?.count > 0{
            return self.timeLineData!.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var dataDictionary = self.timeLineData![indexPath.row]
        
        if ( (dataDictionary[kRating]! as String) == kDefaultRatingText) {
            var cell = tableView.dequeueReusableCellWithIdentifier(kNonRatingCell, forIndexPath: indexPath) as! NonRatingCell
            cell.venueTitle.text = dataDictionary[kName]! as String
            cell.venueAddress.text = dataDictionary[kFormattedAddress]! as String
            return cell
        }else{
            var cell = tableView.dequeueReusableCellWithIdentifier(kRatingCell, forIndexPath: indexPath)as! RatingCell
            cell.venueTitle.text = dataDictionary[kName]! as String
            cell.venueAddress.text = dataDictionary[kFormattedAddress]! as String
            cell.venueRating.text = dataDictionary[kRating]! as String
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

}

