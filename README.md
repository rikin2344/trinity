# README #

iOS application that queries venues and it's ratings around paris. (location Paris hardcoded, can be easily modified).
Uses Reactive Cocoa for internal communication between classes.

PS: Run it on an iPhone 6/6+ or iPhone6/6+ Simulator. (Isn't optimized for smaller screen size devices) 